package ch.matlaw.schooltool;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;

import ch.matlaw.schooltool.database.ClassName;
import ch.matlaw.schooltool.database.DatabaseHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClassesFragment extends Fragment implements View.OnClickListener {

    private DatabaseHelper mDatabaseHelper;

    private CustomAdapter adapter;
    private ArrayList<ClassName> classes;

    private View lastClickedItem;

    public ClassesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mDatabaseHelper = new DatabaseHelper(context);

        adapter = new CustomAdapter();
        classes = new ArrayList<>();
        updateList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_classes, container, false);

        ListView listview_classes = view.findViewById(R.id.listview_classes);
        listview_classes.setAdapter(adapter);

        listview_classes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                lastClickedItem = view;
                return false;
            }
        });

        FloatingActionButton fab_add_class = view.findViewById(R.id.fab_add_class);
        fab_add_class.setOnClickListener(this);

        registerForContextMenu(listview_classes);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab_add_class) {
            openDialog();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context, menu);
        menu.setHeaderTitle(R.string.select_action);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.modify) {
            String name = ((TextView) lastClickedItem.findViewById(R.id.tv_class_name)).getText().toString();
            ClassName className = mDatabaseHelper.getClassFromName(name);
            openDialog(className);
        } else if (item.getItemId() == R.id.delete) {
            String name = ((TextView) lastClickedItem.findViewById(R.id.tv_class_name)).getText().toString();
            ClassName className = mDatabaseHelper.getClassFromName(name);
            mDatabaseHelper.deleteClass(className.getId());
            updateList();
        } else {
            return false;
        }
        return true;
    }

    private void updateList() {
        ArrayList<ClassName> list = mDatabaseHelper.getAllClasses();
        classes.clear();
        classes.addAll(list);
        adapter.notifyDataSetChanged();
    }

    private void openDialog(ClassName className) {
        prepareDialog("Modify", className).create().show();
    }

    private void openDialog() {
        prepareDialog("Add", null).create().show();
    }

    private AlertDialog.Builder prepareDialog(String type, final ClassName className) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(type + " class");

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_manage_classes, null);

        final EditText name = view.findViewById(R.id.editText);

        final Spinner color = view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, new String[] {
                "red",
                "orange",
                "yellow",
                "light_green",
                "green",
                "turquoise",
                "light_blue",
                "blue",
                "violet",
                "purple",
                "fuchsia",
                "magenta",
                "grey"
        });
        color.setAdapter(adapter);

        if (className != null) {
            name.setText(className.getName());
            int col = className.getColor();
            int pos;
            switch (col) {
                case R.color.red:
                    pos = 0;
                    break;
                case R.color.orange:
                    pos = 1;
                    break;
                case R.color.yellow:
                    pos = 2;
                    break;
                case R.color.light_green:
                    pos = 3;
                    break;
                case R.color.green:
                    pos = 4;
                    break;
                case R.color.turquoise:
                    pos = 5;
                    break;
                case R.color.light_blue:
                    pos = 6;
                    break;
                case R.color.blue:
                    pos = 7;
                    break;
                case R.color.violet:
                    pos = 8;
                    break;
                case R.color.purple:
                    pos = 9;
                    break;
                case R.color.fuchsia:
                    pos = 10;
                    break;
                case R.color.magenta:
                    pos = 11;
                    break;
                case R.color.grey:
                    pos = 12;
                    break;
                default:
                    pos = 12;
                    break;
            }
            color.setSelection(pos);
        }

        builder.setView(view);
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String n = name.getText().toString();
                String c = (String) color.getSelectedItem();
                if (n.trim().equals("")) {
                    n = "Unnamed class";
                }
                String origin = n;
                boolean validation = false;
                int num = 1;
                if (className != null) {
                    if (className.getName().equals(n)) {
                        validation = true;
                    }
                }
                while (!validation) {
                    try {
                        if (mDatabaseHelper.getClassFromName(n) != null) {
                            n = origin + num;
                            num++;
                        } else {
                            validation = true;
                        }
                    } catch (Exception ignored) {}
                }
                int col;
                switch (c) {
                    case "red":
                        col = R.color.red;
                        break;
                    case "orange":
                        col = R.color.orange;
                        break;
                    case "yellow":
                        col = R.color.yellow;
                        break;
                    case "light_green":
                        col = R.color.light_green;
                        break;
                    case "green":
                        col = R.color.green;
                        break;
                    case "turquoise":
                        col = R.color.turquoise;
                        break;
                    case "light_blue":
                        col = R.color.light_blue;
                        break;
                    case "blue":
                        col = R.color.blue;
                        break;
                    case "violet":
                        col = R.color.violet;
                        break;
                    case "purple":
                        col = R.color.purple;
                        break;
                    case "fuchsia":
                        col = R.color.fuchsia;
                        break;
                    case "magenta":
                        col = R.color.magenta;
                        break;
                    case "grey":
                        col = R.color.grey;
                        break;
                    default:
                        col = R.color.grey;
                        break;
                }
                if (className == null) {
                    addToList(n, col);
                } else {
                    modifyFromList(className.getName(), n, col);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return builder;
    }

    private void addToList(String name, int color) {
        mDatabaseHelper.addClass(name, color);
        updateList();
    }

    private void modifyFromList(String oldName, String name, int color) {
        mDatabaseHelper.updateClass(mDatabaseHelper.getClassFromName(oldName).getId(), name, color);
        updateList();
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return classes.size();
        }

        @Override
        public Object getItem(int i) {
            return classes.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final ClassName className = classes.get(i);

            View newView = getLayoutInflater().inflate(R.layout.classes_listview_template, null);
            ConstraintLayout cl_class_color = newView.findViewById(R.id.cl_class_color);
            TextView tv_class_name = newView.findViewById(R.id.tv_class_name);

            cl_class_color.setBackgroundColor(getResources().getColor(className.getColor(), getActivity().getTheme()));
            tv_class_name.setText(className.getName());

            return newView;
        }
    }
}
