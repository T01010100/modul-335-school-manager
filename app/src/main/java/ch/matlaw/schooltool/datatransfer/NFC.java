package ch.matlaw.schooltool.datatransfer;

import android.content.Context;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;

public class NFC {
    public static boolean checkNFCConnection(Context context) {
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(context);

        return nfcAdapter != null && nfcAdapter.isEnabled();
    }

    public static void sendMessage(NdefMessage message) {
        sendMessage(message);
    }
}
