package ch.matlaw.schooltool;

import java.util.Calendar;

import ch.matlaw.schooltool.database.Homework;

public class DateComparison {

    public static final int IGNORED = -1;
    public static final int SAME = 0;
    public static final int PAST = 1;
    public static final int LATER = 2;

    private static int compareValues(int selected, int current) {
        if (selected == current) {
            return SAME;
        }
        if (selected < current) {
            return PAST;
        }
        return LATER;
    }

    public static int isToday(Homework homework) {
        int year = compareValues(homework.getYear(), Calendar.getInstance().get(Calendar.YEAR));
        int month = compareValues(homework.getMonth(), Calendar.getInstance().get(Calendar.MONTH));
        int day = compareValues(homework.getDay(), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        if (year == PAST) {
            return PAST;
        }
        if (year == SAME) {
            if (month == PAST) {
                return PAST;
            }
            if (month == SAME) {
                if (day == PAST) {
                    return PAST;
                }
                if (day == SAME) {
                    return SAME;
                }
            }
        }
        return LATER;
    }

    public static int isTomorrow(Homework homework) {
        int year = compareValues(homework.getYear(), Calendar.getInstance().get(Calendar.YEAR));
        int month = compareValues(homework.getMonth(), Calendar.getInstance().get(Calendar.MONTH));
        int day = compareValues(homework.getDay(), Calendar.getInstance().get(Calendar.DAY_OF_MONTH)+1);
        if (year == SAME && month == SAME && day == SAME) {
            return SAME;
        }
        return IGNORED;
    }

    public static int isThisWeek(Homework homework) {
        int cal_year = Calendar.getInstance().get(Calendar.YEAR);
        int cal_month = Calendar.getInstance().get(Calendar.MONTH);
        int cal_day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int cal_date = cal_year*10000 + cal_month*100 + cal_day*3;

        int hom_year = homework.getYear();
        int hom_month = homework.getMonth();
        int hom_day = homework.getDay();
        int hom_date = hom_year*10000 + hom_month*100 + hom_day*3;

        int days = 2*3;
        int week = 10*3;

        if (hom_date < cal_date+days) {
            return PAST;
        }
        if (hom_date > cal_date+week) {
            return LATER;
        }
        return SAME;
    }

    public static int isThisMonth(Homework homework) {
        int cal_year = Calendar.getInstance().get(Calendar.YEAR);
        int cal_month = Calendar.getInstance().get(Calendar.MONTH);
        int cal_day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int cal_date = cal_year*10000 + cal_month*100 + cal_day*3;

        int hom_year = homework.getYear();
        int hom_month = homework.getMonth();
        int hom_day = homework.getDay();
        int hom_date = hom_year*10000 + hom_month*100 + hom_day*3;

        int week = 10*3;
        int month = 100;

        if (hom_date < cal_date) {
            return PAST;
        }
        if (hom_date < cal_date+week) {
            return PAST;
        }
        if (hom_date > cal_date+month) {
            return LATER;
        }
        return SAME;
    }

    public static int later(Homework homework) {
        int year = compareValues(homework.getYear(), Calendar.getInstance().get(Calendar.YEAR));
        int month = compareValues(homework.getMonth(), Calendar.getInstance().get(Calendar.MONTH)+2);

        if (year == LATER) {
            return LATER;
        }
        if (year == SAME && month == LATER) {
            return LATER;
        }
        return IGNORED;
    }
}
