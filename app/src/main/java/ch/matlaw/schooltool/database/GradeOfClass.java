package ch.matlaw.schooltool.database;

import java.util.ArrayList;

public class GradeOfClass {
    private ArrayList<Grade> grades;
    private double average;

    public GradeOfClass(ArrayList<Grade> grades, double average) {
        this.grades = grades;
        this.average = average;
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Grade> grades) {
        this.grades = grades;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
