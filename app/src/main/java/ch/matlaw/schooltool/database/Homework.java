package ch.matlaw.schooltool.database;

public class Homework {
    private int id;
    private ClassName className;
    private String description;
    private boolean completed;
    private int year;
    private int month;
    private int day;
    private int type;

    public static final int HOMEWORK = 0;
    public static final int TEST = 1;
    public static final int PROJECT = 2;

    public Homework(int id, ClassName className, String description, boolean completed, int year, int month, int day, int type) {
        this.id = id;
        this.className = className;
        this.description = description;
        this.completed = completed;
        this.year = year;
        this.month = month;
        this.day = day;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ClassName getClassName() {
        return className;
    }

    public void setClassName(ClassName className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
