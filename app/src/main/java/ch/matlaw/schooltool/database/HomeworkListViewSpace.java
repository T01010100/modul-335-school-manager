package ch.matlaw.schooltool.database;

public class HomeworkListViewSpace extends Homework {
    public HomeworkListViewSpace() {
        super(-1, null, "", false, 1, 1, 1, 0);
    }
    public HomeworkListViewSpace(int id, ClassName className, String description, boolean completed, int year, int month, int day, int type) {
        super(id, className, description, completed, year, month, day, type);
    }
}
