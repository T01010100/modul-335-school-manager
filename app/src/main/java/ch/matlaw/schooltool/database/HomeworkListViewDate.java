package ch.matlaw.schooltool.database;

public class HomeworkListViewDate extends Homework {
    private String message;

    public HomeworkListViewDate(String message) {
        super(-1, null, "", false, 1, 1, 1, 0);
        this.message = message;
    }

    public HomeworkListViewDate(int id, ClassName className, String description, boolean completed, int year, int month, int day, int type) {
        super(id, className, description, completed, year, month, day, type);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
