package ch.matlaw.schooltool.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "school_manager";
    public static final int DB_VERSION = 2;

    public static final String TABLE_CLASSES = "classes";
    public static final String COLUMN_CLASSES_ID = "class_id";
    public static final String COLUMN_CLASSES_NAME = "name";
    public static final String COLUMN_CLASSES_COLOR = "color";

    public static final String TABLE_GRADES = "grades";
    public static final String COLUMN_GRADES_ID = "grade_id";
    public static final String COLUMN_GRADES_CLASS = "class_fk";
    public static final String COLUMN_GRADES_EXAM = "exam";
    public static final String COLUMN_GRADES_VALUE = "value";
    public static final String COLUMN_GRADES_WEIGHT = "weight";

    public static final String TABLE_HOMEWORK = "homework";
    public static final String COLUMN_HOMEWORK_ID = "homework_id";
    public static final String COLUMN_HOMEWORK_CLASS = "class_fk";
    public static final String COLUMN_HOMEWORK_DESCRIPTION = "description";
    public static final String COLUMN_HOMEWORK_COMPLETED = "completed";
    public static final String COLUMN_HOMEWORK_YEAR = "year";
    public static final String COLUMN_HOMEWORK_MONTH = "month";
    public static final String COLUMN_HOMEWORK_DAY = "day";
    public static final String COLUMN_HOMEWORK_TYPE = "type";

    private static final String CREATE_TABLE_CLASSES = "CREATE TABLE " + TABLE_CLASSES
            + " (" + COLUMN_CLASSES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_CLASSES_NAME + " TEXT, "
            + COLUMN_CLASSES_COLOR + " INTEGER);";
    private static final String CREATE_TABLE_GRADES = "CREATE TABLE " + TABLE_GRADES
            + " (" + COLUMN_GRADES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_GRADES_CLASS + " INTEGER, "
            + COLUMN_GRADES_EXAM + " TEXT, " + COLUMN_GRADES_VALUE + " DECIMAL, "
            + COLUMN_GRADES_WEIGHT + " DECIMAL, "
            + "FOREIGN KEY (" + COLUMN_GRADES_CLASS + ") REFERENCES " + TABLE_CLASSES + " (" + COLUMN_CLASSES_ID + "));";
    private static final String CREATE_TABLE_HOMEWORK = "CREATE TABLE " + TABLE_HOMEWORK
            + " (" + COLUMN_HOMEWORK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_HOMEWORK_CLASS + " INTEGER, "
            + COLUMN_HOMEWORK_DESCRIPTION + " TEXT, " + COLUMN_HOMEWORK_COMPLETED + " INTEGER, "
            + COLUMN_HOMEWORK_YEAR + " INTEGER, " + COLUMN_HOMEWORK_MONTH + " INTEGER, "
            + COLUMN_HOMEWORK_DAY + " INTEGER, " + COLUMN_HOMEWORK_TYPE + " INTEGER, "
            + "FOREIGN KEY (" + COLUMN_HOMEWORK_CLASS + ") REFERENCES " + TABLE_CLASSES + " (" + COLUMN_CLASSES_ID + "));";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_CLASSES);
        sqLiteDatabase.execSQL(CREATE_TABLE_GRADES);
        sqLiteDatabase.execSQL(CREATE_TABLE_HOMEWORK);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + TABLE_CLASSES + "';");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + TABLE_GRADES + "';");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + TABLE_HOMEWORK + "';");
        onCreate(sqLiteDatabase);
    }

    public void addClass(String name, int color) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("INSERT INTO " + TABLE_CLASSES + " (" + COLUMN_CLASSES_NAME + ", " + COLUMN_CLASSES_COLOR
                + ") VALUES ('" + name + "', '" + color + "');");
    }

    public ArrayList<ClassName> getAllClasses() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ClassName> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_CLASSES + ";",
                null
        );
        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_ID));
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_CLASSES_NAME));
                int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
                result.add(new ClassName(id, name, color));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return result;
    }

    public ClassName getClassFromId(int index) {
        SQLiteDatabase db = this.getReadableDatabase();

        ClassName result = null;
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_CLASSES + " WHERE " + COLUMN_CLASSES_ID + "=" + index + ";",
                null
        );
        if (cursor.moveToFirst()) {
            String name = cursor.getString(cursor.getColumnIndex(COLUMN_CLASSES_NAME));
            int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
            result = new ClassName(index, name, color);
        }
        cursor.close();

        return result;
    }

    public ClassName getClassFromName(String name) {
        SQLiteDatabase db = this.getReadableDatabase();

        ClassName result = null;
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_CLASSES + " WHERE " + COLUMN_CLASSES_NAME + "='" + name + "';",
                null
        );
        if (cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_ID));
            int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
            result = new ClassName(id, name, color);
        }
        cursor.close();

        return result;
    }

    public void updateClass(int id, String name, int color) {
        SQLiteDatabase db = this.getWritableDatabase();

        ClassName previous = getClassFromId(id);
        if (previous == null) {
            return;
        }
        if (!previous.getName().equals(name)) {
            db.execSQL("UPDATE " + TABLE_CLASSES + " SET " + COLUMN_CLASSES_NAME + "='" + name + "' WHERE " + COLUMN_CLASSES_ID + "=" + id + ";");
        }
        if (previous.getColor() != (color)) {
            db.execSQL("UPDATE " + TABLE_CLASSES + " SET " + COLUMN_CLASSES_COLOR + "='" + color + "' WHERE " + COLUMN_CLASSES_ID + "=" + id + ";");
        }
    }

    public void deleteClass(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_CLASSES + " WHERE " + COLUMN_CLASSES_ID + "=" + id + ";");
    }

    public void addGrade(int class_fk, String exam, double value, double weight) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("INSERT INTO " + TABLE_GRADES + " (" + COLUMN_GRADES_CLASS + ", " + COLUMN_GRADES_EXAM
                + ", " + COLUMN_GRADES_VALUE + ", " + COLUMN_GRADES_WEIGHT
                + ") VALUES ('" + class_fk + "', '" + exam + "', '" + value + "', '" + weight + "');");
    }

    public ArrayList<Grade> getAllGrades() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Grade> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_GRADES + " JOIN " + TABLE_CLASSES + " ON " + TABLE_GRADES + "." + COLUMN_GRADES_CLASS + "=" + COLUMN_CLASSES_ID + ";",
                null
        );
        if (cursor.moveToFirst()) {
            do {
                int grade_id = cursor.getInt(cursor.getColumnIndex(COLUMN_GRADES_ID));
                int class_id = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_ID));
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_CLASSES_NAME));
                int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
                String exam = cursor.getString(cursor.getColumnIndex(COLUMN_GRADES_EXAM));
                double value = cursor.getDouble(cursor.getColumnIndex(COLUMN_GRADES_VALUE));
                double weight = cursor.getDouble(cursor.getColumnIndex(COLUMN_GRADES_WEIGHT));
                result.add(new Grade(grade_id, new ClassName(class_id, name, color), exam, value, weight));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return result;
    }

    public GradeOfClass getGradesFromClass(ClassName className) {
        SQLiteDatabase db = this.getReadableDatabase();

        GradeOfClass result;
        ArrayList<Grade> grades = new ArrayList<>();
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_GRADES + " JOIN " + TABLE_CLASSES + " ON " + TABLE_GRADES + "." + COLUMN_GRADES_CLASS + "=" + TABLE_CLASSES + "." + COLUMN_CLASSES_ID + " WHERE " + COLUMN_CLASSES_ID + "=" + className.getId() + ";",
                null
        );
        if (cursor.moveToFirst()) {
            do {
                int grade_id = cursor.getInt(cursor.getColumnIndex(COLUMN_GRADES_ID));
                int class_id = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_ID));
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_CLASSES_NAME));
                int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
                String exam = cursor.getString(cursor.getColumnIndex(COLUMN_GRADES_EXAM));
                double value = cursor.getDouble(cursor.getColumnIndex(COLUMN_GRADES_VALUE));
                double weight = cursor.getDouble(cursor.getColumnIndex(COLUMN_GRADES_WEIGHT));
                grades.add(new Grade(grade_id, new ClassName(class_id, name, color), exam, value, weight));
            } while (cursor.moveToNext());
        }
        cursor.close();

        double avg = calculateAverage(grades);
        result = new GradeOfClass(grades, round(avg));

        return result;
    }

    private double calculateAverage(ArrayList<Grade> grades) {
        double totalWeight = 0;
        for (Grade g : grades) {
            totalWeight += g.getWeight();
        }
        double weightOffset = 1/totalWeight;
        double avg = 0;
        for (Grade g : grades) {
            avg += g.getValue()*g.getWeight()*weightOffset;
        }
        return avg;
    }

    private double round(double value) {
        double temp = value * 1000;
        double round = Math.round(temp);
        return round/1000;
    }

    public ArrayList<GradeOfClass> getAllGradesFromClasses() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ClassName> classes = this.getAllClasses();
        ArrayList<GradeOfClass> result = new ArrayList<>();

        for (ClassName className : classes) {
            result.add(getGradesFromClass(className));
        }

        return result;
    }

    public Grade getGradeFromId(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Grade result = null;
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_GRADES + " JOIN " + TABLE_CLASSES + " ON " + TABLE_GRADES + "." + COLUMN_GRADES_CLASS + "=" + TABLE_CLASSES + "." + COLUMN_CLASSES_ID + " WHERE " + COLUMN_GRADES_ID + "=" + id + ";",
                null
        );
        if (cursor.moveToFirst()) {
            int grade_id = cursor.getInt(cursor.getColumnIndex(COLUMN_GRADES_ID));
            int class_id = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_ID));
            String name = cursor.getString(cursor.getColumnIndex(COLUMN_CLASSES_NAME));
            int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
            String exam = cursor.getString(cursor.getColumnIndex(COLUMN_GRADES_EXAM));
            double value = cursor.getDouble(cursor.getColumnIndex(COLUMN_GRADES_VALUE));
            double weight = cursor.getDouble(cursor.getColumnIndex(COLUMN_GRADES_WEIGHT));
            result = new Grade(grade_id, new ClassName(class_id, name, color), exam, value, weight);
        }
        cursor.close();

        return result;
    }

    public Grade getGradeFromName(String exam) {
        SQLiteDatabase db = this.getReadableDatabase();

        Grade result = null;
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_GRADES + " JOIN " + TABLE_CLASSES + " ON " + TABLE_GRADES + "." + COLUMN_GRADES_CLASS + "=" + TABLE_CLASSES + "." + COLUMN_CLASSES_ID + " WHERE " + COLUMN_GRADES_EXAM + "='" + exam + "';",
                null
        );
        if (cursor.moveToFirst()) {
            int grade_id = cursor.getInt(cursor.getColumnIndex(COLUMN_GRADES_ID));
            int class_id = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_ID));
            String name = cursor.getString(cursor.getColumnIndex(COLUMN_CLASSES_NAME));
            int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
            double value = cursor.getDouble(cursor.getColumnIndex(COLUMN_GRADES_VALUE));
            double weight = cursor.getDouble(cursor.getColumnIndex(COLUMN_GRADES_WEIGHT));
            result = new Grade(grade_id, new ClassName(class_id, name, color), exam, value, weight);
        }
        cursor.close();

        return result;
    }

    public void updateGrade(int id, String exam, double value, double weight) {
        SQLiteDatabase db = this.getWritableDatabase();

        Grade previous = getGradeFromId(id);
        if (previous == null) {
            return;
        }
        if (!previous.getExam().equals(exam)) {
            db.execSQL("UPDATE " + TABLE_GRADES + " SET " + COLUMN_GRADES_EXAM + "='" + exam + "' WHERE " + COLUMN_GRADES_ID + "=" + id + ";");
        }
        if (previous.getValue() != (value)) {
            db.execSQL("UPDATE " + TABLE_GRADES + " SET " + COLUMN_GRADES_VALUE + "='" + value + "' WHERE " + COLUMN_GRADES_ID + "=" + id + ";");
        }
        if (previous.getWeight() != (weight)) {
            db.execSQL("UPDATE " + TABLE_GRADES + " SET " + COLUMN_GRADES_WEIGHT + "='" + weight + "' WHERE " + COLUMN_GRADES_ID + "=" + id + ";");
        }
    }

    public void deleteGrade(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_GRADES + " WHERE " + COLUMN_GRADES_ID + "=" + id + ";");
    }

    public void addHomework(int class_fk, String description, boolean completed, int year, int month, int day, int type) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("INSERT INTO " + TABLE_HOMEWORK + " (" + COLUMN_HOMEWORK_CLASS + ", " + COLUMN_HOMEWORK_DESCRIPTION + ", "
                + COLUMN_HOMEWORK_COMPLETED + ", " + COLUMN_HOMEWORK_YEAR + ", " + COLUMN_HOMEWORK_MONTH + ", " + COLUMN_HOMEWORK_DAY + ", "
                + COLUMN_HOMEWORK_TYPE + ") VALUES ('" + class_fk + "', '" + description + "', '" + completed + "', '" + year + "', '" + month + "', '" + day + "', '" + type + "');");
    }

    public ArrayList<Homework> getAllHomework() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Homework> result = new ArrayList<>();
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_HOMEWORK + " JOIN " + TABLE_CLASSES + " ON " + COLUMN_HOMEWORK_CLASS + "=" + COLUMN_CLASSES_ID
                        + " WHERE " + COLUMN_HOMEWORK_COMPLETED + "='false'"
                        + " ORDER BY " + COLUMN_HOMEWORK_YEAR + " ASC, " + COLUMN_HOMEWORK_MONTH + " ASC, " + COLUMN_HOMEWORK_DAY + " ASC;",
                null
        );
        if (cursor.moveToFirst()) {
            do {
                int homework_id = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_ID));
                int class_id = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_ID));
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_CLASSES_NAME));
                int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
                String description = cursor.getString(cursor.getColumnIndex(COLUMN_HOMEWORK_DESCRIPTION));
                boolean completed = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_COMPLETED)) == 1;
                int year = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_YEAR));
                int month = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_MONTH));
                int day = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_DAY));
                int type = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_TYPE));
                result.add(new Homework(homework_id, new ClassName(class_id, name, color), description, completed, year, month, day, type));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return result;
    }

    public Homework getHomeworkFromId(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Homework result = null;
        Cursor cursor = db.rawQuery(
                "SELECT * FROM " + TABLE_HOMEWORK + " JOIN " + TABLE_CLASSES + " ON " + COLUMN_HOMEWORK_CLASS + "=" + COLUMN_CLASSES_ID + " WHERE " + COLUMN_HOMEWORK_ID + "=" + id + ";",
                null
        );
        if (cursor.moveToFirst()) {
            int homework_id = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_ID));
            int class_id = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_ID));
            String name = cursor.getString(cursor.getColumnIndex(COLUMN_CLASSES_NAME));
            int color = cursor.getInt(cursor.getColumnIndex(COLUMN_CLASSES_COLOR));
            String description = cursor.getString(cursor.getColumnIndex(COLUMN_HOMEWORK_DESCRIPTION));
            boolean completed = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_COMPLETED)) == 1;
            int year = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_YEAR));
            int month = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_MONTH));
            int day = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_DAY));
            int type = cursor.getInt(cursor.getColumnIndex(COLUMN_HOMEWORK_TYPE));
            result = new Homework(homework_id, new ClassName(class_id, name, color), description, completed, year, month, day, type);
        }
        cursor.close();

        return result;
    }

    public void updateHomework(int id, String description, boolean completed, int year, int month, int day, int type) {
        SQLiteDatabase db = this.getWritableDatabase();

        Homework previous = getHomeworkFromId(id);
        if (previous == null) {
            return;
        }
        if (!previous.getDescription().equals(description)) {
            db.execSQL("UPDATE " + TABLE_HOMEWORK + " SET " + COLUMN_HOMEWORK_DESCRIPTION + "='" + description + "' WHERE " + COLUMN_HOMEWORK_ID + "=" + id + ";");
        }
        if (previous.isCompleted() != completed) {
            db.execSQL("UPDATE " + TABLE_HOMEWORK + " SET " + COLUMN_HOMEWORK_COMPLETED + "='" + completed + "' WHERE " + COLUMN_HOMEWORK_ID + "=" + id + ";");
        }
        if (previous.getYear() != year) {
            db.execSQL("UPDATE " + TABLE_HOMEWORK + " SET " + COLUMN_HOMEWORK_YEAR + "='" + year + "' WHERE " + COLUMN_HOMEWORK_ID + "=" + id + ";");
        }
        if (previous.getMonth() != month) {
            db.execSQL("UPDATE " + TABLE_HOMEWORK + " SET " + COLUMN_HOMEWORK_MONTH + "='" + month + "' WHERE " + COLUMN_HOMEWORK_ID + "=" + id + ";");
        }
        if (previous.getDay() != day) {
            db.execSQL("UPDATE " + TABLE_HOMEWORK + " SET " + COLUMN_HOMEWORK_DAY + "='" + day + "' WHERE " + COLUMN_HOMEWORK_ID + "=" + id + ";");
        }
        if (previous.getType() != type) {
            db.execSQL("UPDATE " + TABLE_HOMEWORK + " SET " + COLUMN_HOMEWORK_TYPE + "='" + type + "' WHERE " + COLUMN_HOMEWORK_ID + "=" + id + ";");
        }
    }

    public void deleteHomework(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_HOMEWORK + " WHERE " + COLUMN_HOMEWORK_ID + "=" + id + ";");
    }
}
