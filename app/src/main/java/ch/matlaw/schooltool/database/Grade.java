package ch.matlaw.schooltool.database;

public class Grade {
    private int id;
    private ClassName className;
    private String exam;
    private double value;
    private double weight;

    public Grade(int id, ClassName className, String exam, double value, double weight) {
        this.id = id;
        this.className = className;
        this.exam = exam;
        this.value = value;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ClassName getClassName() {
        return className;
    }

    public void setClassName(ClassName className) {
        this.className = className;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
