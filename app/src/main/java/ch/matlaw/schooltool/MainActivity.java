package ch.matlaw.schooltool;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Calendar;

import static ch.matlaw.schooltool.App.CHANNEL_HOMEWORK_ID;

public class MainActivity extends AppCompatActivity {

    private NotificationManagerCompat notificationManager;
    private static MainActivity instance;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            menuItem.setChecked(true);

            FragmentTransaction fragments = getSupportFragmentManager().beginTransaction();
            switch (menuItem.getItemId()) {
                case R.id.navigation_homework:
                    fragments.replace(R.id.fragment_layout, new HomeworkFragment());
                    setTitle(R.string.navigation_homework);
                    fragments.commit();
                    break;
                case R.id.navigation_grades:
                    fragments.replace(R.id.fragment_layout, new GradesFragment());
                    setTitle(R.string.navigation_grades);
                    fragments.commit();
                    break;
                case R.id.navigation_classes:
                    fragments.replace(R.id.fragment_layout, new ClassesFragment());
                    setTitle(R.string.navigation_classes);
                    fragments.commit();
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentTransaction fragments = getSupportFragmentManager().beginTransaction();
        fragments.replace(R.id.fragment_layout, new HomeworkFragment());
        setTitle(R.string.navigation_homework);
        fragments.commit();

        BottomNavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setSelectedItemId(R.id.navigation_homework);
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        instance = this;
        notificationManager = NotificationManagerCompat.from(this);

        Calendar alarmStartTime = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        alarmStartTime.set(Calendar.HOUR_OF_DAY, 11);
        alarmStartTime.set(Calendar.MINUTE, 00);
        alarmStartTime.set(Calendar.SECOND, 0);
        if (now.after(alarmStartTime)) {
            alarmStartTime.add(Calendar.DATE, 1);
        }

        Intent notifyIntent = new Intent(this, MyReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast
                (getApplicationContext(), NotificationManager.IMPORTANCE_DEFAULT, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  System.currentTimeMillis(),
        //        1000 * 60, pendingIntent);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  alarmStartTime.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    @Override
    public void onBackPressed() {
        try {
            GradesFragment test = (GradesFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
            if (test != null && test.isVisible()) {
                test.updateGradeList();
            }
        } catch (Exception ignored) {}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.vertical, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.about:
                /*FragmentTransaction fragments = getSupportFragmentManager().beginTransaction();
                fragments.replace(R.id.fragment_layout, new AboutFragment());
                setTitle(R.string.navigation_about);
                fragments.commit();*/
                return true;
        }
        return true;
    }

    public void sendOnChannelHomework(PendingIntent pendingIntent, String title, String message) {
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_HOMEWORK_ID)
                .setSmallIcon(R.drawable.ic_baseline_web_24px)
                .setContentTitle(title)
                .setContentText(message)
                .setChannelId(CHANNEL_HOMEWORK_ID)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .build();

        notificationManager.notify(1, notification);
    }

    public static class MyReceiver extends BroadcastReceiver {
        public MyReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            Intent intent1 = new Intent(context, MyNewIntentService.class);
            context.startService(intent1);
        }
    }

    public static class MyNewIntentService extends IntentService {

        public MyNewIntentService() {
            super("MyNewIntentService");
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            Intent notifyIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 2, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            instance.sendOnChannelHomework(pendingIntent,"Test", "test jo lo");
        }
    }
}
