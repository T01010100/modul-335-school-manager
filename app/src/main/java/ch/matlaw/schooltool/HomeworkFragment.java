package ch.matlaw.schooltool;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ch.matlaw.schooltool.database.ClassName;
import ch.matlaw.schooltool.database.DatabaseHelper;
import ch.matlaw.schooltool.database.Homework;
import ch.matlaw.schooltool.database.HomeworkListViewDate;
import ch.matlaw.schooltool.database.HomeworkListViewSpace;
import ch.matlaw.schooltool.datatransfer.NFC;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeworkFragment extends Fragment {

    private DatabaseHelper mDatabaseHelper;

    private CustomAdapter adapter;
    private ArrayList<Homework> homework;

    private View lastClickedItem;

    public HomeworkFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mDatabaseHelper = new DatabaseHelper(context);

        adapter = new CustomAdapter();
        homework = new ArrayList<>();
        updateList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_homework, container, false);

        ListView listview_homework = view.findViewById(R.id.listview_homework);
        listview_homework.setAdapter(adapter);

        listview_homework.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                lastClickedItem = view;
                return false;
            }
        });

        FloatingActionButton fab_add_homework = view.findViewById(R.id.fab_add_homework);
        fab_add_homework.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });

        registerForContextMenu(listview_homework);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.homework_context, menu);
        menu.setHeaderTitle(R.string.select_action);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        String hidden_id = ((TextView) lastClickedItem.findViewById(R.id.tv_hidden_id)).getText().toString();
        Homework hwk = mDatabaseHelper.getHomeworkFromId(Integer.parseInt(hidden_id));
        switch (item.getItemId()) {
            case R.id.hc_done:
                mDatabaseHelper.updateHomework(hwk.getId(), hwk.getDescription(), true, hwk.getYear(), hwk.getMonth(), hwk.getDay(), hwk.getType());
                updateList();
                break;
            case R.id.hc_modify:
                openDialog(hwk);
                break;
            case R.id.hc_send:
                sendHomework(hwk);
                break;
            case R.id.hc_delete:
                mDatabaseHelper.deleteHomework(hwk.getId());
                updateList();
                break;
            default:
                return false;
        }
        return true;
    }

    private void updateList() {
        ArrayList<Homework> list = mDatabaseHelper.getAllHomework();


        ArrayList<Integer> wasAlready = new ArrayList<>();

        int stat = -1;
        for (int i = 0; i < list.size(); i++) {
            Homework obj = list.get(i);
            if (stat < 0 && DateComparison.isToday(obj) == DateComparison.PAST) {
                list.add(i, new HomeworkListViewDate(getResources().getString(R.string.overdue)));
                stat = 0;
            }
            if (stat < 1 && DateComparison.isToday(obj) == DateComparison.SAME) {
                list.add(i, new HomeworkListViewDate(getResources().getString(R.string.today)));
                stat = 1;
            }
            if (stat < 2 && DateComparison.isTomorrow(obj) == DateComparison.SAME) {
                list.add(i, new HomeworkListViewDate(getResources().getString(R.string.tomorrow)));
                stat = 2;
            }
            if (stat < 3 && DateComparison.isThisWeek(obj) == DateComparison.SAME) {
                Calendar c = Calendar.getInstance();
                Date myDate = Date.from(LocalDate.of( obj.getYear(), obj.getMonth()+1, obj.getDay()).atStartOfDay(ZoneId.of( "Europe/Berlin" )).toInstant());
                c.setTime(myDate);

                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                if (!wasAlready.contains(dayOfWeek)) {
                    wasAlready.add(dayOfWeek);
                    switch (dayOfWeek) {
                        case 1:
                            list.add(i, new HomeworkListViewDate(getResources().getString(R.string.sunday)));
                            break;
                        case 2:
                            list.add(i, new HomeworkListViewDate(getResources().getString(R.string.monday)));
                            break;
                        case 3:
                            list.add(i, new HomeworkListViewDate(getResources().getString(R.string.tuesday)));
                            break;
                        case 4:
                            list.add(i, new HomeworkListViewDate(getResources().getString(R.string.wednesday)));
                            break;
                        case 5:
                            list.add(i, new HomeworkListViewDate(getResources().getString(R.string.thursday)));
                            break;
                        case 6:
                            list.add(i, new HomeworkListViewDate(getResources().getString(R.string.friday)));
                            break;
                        case 7:
                            list.add(i, new HomeworkListViewDate(getResources().getString(R.string.saturday)));
                            break;
                    }
                }
            }
            if (stat < 3 && DateComparison.isThisMonth(obj) == DateComparison.SAME) {
                list.add(i, new HomeworkListViewDate(getResources().getString(R.string.this_month)));
                stat = 3;
            }
            if (stat < 4 && DateComparison.isThisMonth(obj) == DateComparison.LATER) {
                list.add(i, new HomeworkListViewDate(getResources().getString(R.string.next_month)));
                stat = 4;
            }
            if (stat < 5 && DateComparison.later(obj) == DateComparison.LATER) {
                list.add(i, new HomeworkListViewDate(getResources().getString(R.string.later)));
                stat = 5;
            }
        }
        list.add(new HomeworkListViewSpace());

        homework.clear();
        homework.addAll(list);
        adapter.notifyDataSetChanged();
    }

    private void sendHomework(Homework homework) {
        Intent intent = new Intent(getContext(), NFCActivity.class);
        //intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

        return;
        /*if (NFC.checkNFCConnection(getContext())) {
            Toast.makeText(getContext(), "NFC working", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "NFC not working", Toast.LENGTH_LONG).show();
        }*/
    }

    private void openDialog(Homework homework) {
        prepareDialog(getResources().getString(R.string.modify), homework).create().show();
    }

    private void openDialog() {
        prepareDialog(getResources().getString(R.string.add), null).create().show();
    }

    private AlertDialog.Builder prepareDialog(String type, final Homework homework) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(type + " " + getResources().getString(R.string.homework));

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_manage_homework, null);

        final EditText et_desc = view.findViewById(R.id.edittext_homework_description);
        final EditText et_date = view.findViewById(R.id.edittext_homework_due_date);
        et_date.setFocusable(false);

        final int[] dt = new int[3];

        final Calendar myCalendar = Calendar.getInstance();
        final String[] myFormat = {"dd/MM/yyyy"};
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat[0], Locale.GERMANY);
        myCalendar.set(Calendar.DAY_OF_MONTH, myCalendar.get(Calendar.DAY_OF_MONTH)+1);
        et_date.setText(sdf.format(myCalendar.getTime()));
        if (homework == null) {
            dt[0] = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + 1;
            dt[1] = Calendar.getInstance().get(Calendar.MONTH);
            dt[2] = Calendar.getInstance().get(Calendar.YEAR);
        } else {
            dt[0] = homework.getDay();
            dt[1] = homework.getMonth();
            dt[2] = homework.getYear();
        }
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                dt[0] = dayOfMonth;
                dt[1] = monthOfYear;
                dt[2] = year;

                String myFormat = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMANY);

                et_date.setText(sdf.format(myCalendar.getTime()));
            }
        };
        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (homework == null) {
                    new DatePickerDialog(getContext(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                } else {
                    new DatePickerDialog(getContext(), date, dt[2], dt[1], dt[0]).show();
                }
            }
        });

        final RadioGroup rg_type = view.findViewById(R.id.rg_type);

        final Spinner s_class = view.findViewById(R.id.spinner_homework_class);

        ArrayList<String> classNames = new ArrayList<>();
        for (ClassName cls : mDatabaseHelper.getAllClasses()) {
            classNames.add(cls.getName());
        }
        final String[] classes = new String[classNames.size()];
        for (int i = 0; i < classNames.size(); i++) {
            classes[i] = classNames.get(i);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, classes);
        s_class.setAdapter(adapter);

        if (homework != null) {
            String tempClassName = homework.getClassName().getName();
            et_desc.setText(homework.getDescription());
            et_date.setText(homework.getDay() + "/" + (homework.getMonth()+1) + "/" + homework.getYear());

            int pos = 0;
            for (int i = 0; i < classes.length; i++) {
                if (classes[i].equals(tempClassName)) {
                    pos = i;
                    break;
                }
            }

            s_class.setSelection(pos);
        }

        builder.setView(view);
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String cls = (String) s_class.getSelectedItem();
                String dsc = et_desc.getText().toString();
                if (dsc.trim().equals("")) {
                    dsc = "Unknown work";
                }
                int j = rg_type.getCheckedRadioButtonId();
                int typ;
                switch (j) {
                    case R.id.rb_homework:
                        typ = Homework.HOMEWORK;
                        break;
                    case R.id.rb_test:
                        typ = Homework.TEST;
                        break;
                    case R.id.rb_project:
                        typ = Homework.PROJECT;
                        break;
                    default:
                        typ = Homework.HOMEWORK;
                        break;
                }

                int class_fk = 1;
                for (ClassName cN : mDatabaseHelper.getAllClasses()) {
                    if (cN.getName().equals(cls)) {
                        class_fk = cN.getId();
                        break;
                    }
                }
                if (homework == null) {
                    addToList(mDatabaseHelper.getClassFromId(class_fk), dsc, false, dt[2], dt[1], dt[0], typ);
                } else {
                    modifyFromList(homework.getId(), dsc, false, dt[2], dt[1], dt[0], typ);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return builder;
    }

    private void addToList(ClassName className, String description, boolean completed, int year, int month, int day, int type) {
        mDatabaseHelper.addHomework(className.getId(), description, completed, year, month, day, type);
        updateList();
    }

    private void modifyFromList(int id, String description, boolean completed, int year, int month, int day, int type) {
        mDatabaseHelper.updateHomework(id, description, completed, year, month, day, type);
        updateList();
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return homework.size();
        }

        @Override
        public Object getItem(int i) {
            return homework.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final Homework hwk = homework.get(i);

            if (hwk instanceof HomeworkListViewDate) {
                View newView = getLayoutInflater().inflate(R.layout.homework_date_listview_template, null);
                ConstraintLayout cl_homework_date_background = newView.findViewById(R.id.cl_homework_date_background);
                TextView tv_homework_date_time = newView.findViewById(R.id.tv_homework_date_time);

                cl_homework_date_background.setBackgroundColor(getResources().getColor(R.color.light_grey, getActivity().getTheme()));
                tv_homework_date_time.setText(((HomeworkListViewDate) hwk).getMessage());
                tv_homework_date_time.bringToFront();

                return newView;
            } else if (hwk instanceof HomeworkListViewSpace) {
                return getLayoutInflater().inflate(R.layout.homework_space_listview_template, null);
            }

            View newView = getLayoutInflater().inflate(R.layout.homework_listview_template, null);
            ConstraintLayout cl_homework_color = newView.findViewById(R.id.cl_homework_color);
            TextView tv_homework_class = newView.findViewById(R.id.tv_homework_class);
            TextView tv_homework_desc = newView.findViewById(R.id.tv_homework_desc);
            TextView tv_homework_date = newView.findViewById(R.id.tv_homework_date);
            TextView tv_hidden_id = newView.findViewById(R.id.tv_hidden_id);

            tv_hidden_id.setText(""+hwk.getId());

            ImageView iv_exam = newView.findViewById(R.id.iv_exam);

            cl_homework_color.setBackgroundColor(getResources().getColor(hwk.getClassName().getColor(), getActivity().getTheme()));
            switch (hwk.getType()) {
                case Homework.TEST:
                    tv_homework_class.setText(hwk.getClassName().getName() + " " + getResources().getString(R.string.test));
                    iv_exam.setVisibility(View.VISIBLE);
                    break;
                case Homework.PROJECT:
                    tv_homework_class.setText(hwk.getClassName().getName() + " " + getResources().getString(R.string.project));
                    iv_exam.setVisibility(View.VISIBLE);
                    break;
                default:
                    tv_homework_class.setText(hwk.getClassName().getName());
                    break;
            }
            tv_homework_desc.setText(hwk.getDescription());
            tv_homework_date.setText(hwk.getDay() + "/" + (hwk.getMonth()+1));

            return newView;
        }
    }
}
