package ch.matlaw.schooltool;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class App extends Application {
    public static final String CHANNEL_HOMEWORK_ID = "channel_for_homework_notifications_only";

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
            NotificationChannel homework_channel = new NotificationChannel(CHANNEL_HOMEWORK_ID, "Homework notifications", NotificationManager.IMPORTANCE_HIGH);
            homework_channel.setDescription("Homework notifications");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(homework_channel);
        }
    }
}
