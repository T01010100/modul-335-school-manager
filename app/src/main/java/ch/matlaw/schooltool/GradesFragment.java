package ch.matlaw.schooltool;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;

import ch.matlaw.schooltool.database.ClassName;
import ch.matlaw.schooltool.database.DatabaseHelper;
import ch.matlaw.schooltool.database.Grade;
import ch.matlaw.schooltool.database.GradeOfClass;


/**
 * A simple {@link Fragment} subclass.
 */
public class GradesFragment extends Fragment {

    private DatabaseHelper mDatabaseHelper;

    private ListView lvAG;
    private ListView lvSG;
    private ImageButton ib_back;

    private CustomGradesAdapter grades_adapter;
    private CustomSingleGradesAdapter single_grades_adapter;
    private ArrayList<GradeOfClass> gradeOfClasses;
    private ArrayList<Grade> single_grades;

    private String status;

    private View lastClickedItem;

    public GradesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mDatabaseHelper = new DatabaseHelper(context);

        status = "null";

        grades_adapter = new CustomGradesAdapter();
        single_grades_adapter = new CustomSingleGradesAdapter();
        gradeOfClasses = new ArrayList<>();
        single_grades = new ArrayList<>();
        updateGradeList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grades, container, false);

        ListView listview_grades = view.findViewById(R.id.listview_grades);
        listview_grades.setAdapter(grades_adapter);

        ListView listview_single_grades = view.findViewById(R.id.listview_single_grades);
        listview_single_grades.setAdapter(single_grades_adapter);

        lvAG = listview_grades;
        lvSG = listview_single_grades;

        listview_grades.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String clsNm = ((TextView) view.findViewById(R.id.tv_grade_name)).getText().toString();
                ClassName className = mDatabaseHelper.getClassFromName(clsNm);
                ArrayList<Grade> selectedGrades = mDatabaseHelper.getGradesFromClass(className).getGrades();
                updateSingleGradeList(selectedGrades);
            }
        });

        // TODO NOPE TIS IS NOT NEEDED PUT THIS IN OTHER
        listview_single_grades.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                lastClickedItem = view;
                return false;
            }
        });

        FloatingActionButton fab_add_grade = view.findViewById(R.id.fab_add_grade);
        fab_add_grade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });

        registerForContextMenu(listview_single_grades);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context, menu);
        menu.setHeaderTitle(R.string.select_action);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.modify) {
            String name = ((TextView) lastClickedItem.findViewById(R.id.tv_single_grade_title)).getText().toString();
            Grade grade = mDatabaseHelper.getGradeFromName(name);
            openDialog(grade);
        } else if (item.getItemId() == R.id.delete) {
            String name = ((TextView) lastClickedItem.findViewById(R.id.tv_single_grade_title)).getText().toString();
            Grade grade = mDatabaseHelper.getGradeFromName(name);
            mDatabaseHelper.deleteGrade(grade.getId());
            updateSingleGradeList(mDatabaseHelper.getGradesFromClass(mDatabaseHelper.getClassFromName(status)).getGrades());
        } else {
            return false;
        }
        return true;
    }

    public void updateGradeList() {
        ArrayList<GradeOfClass> list = mDatabaseHelper.getAllGradesFromClasses();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getGrades().size() == 0) {
                list.remove(i);
                i--;
            }
        }
        gradeOfClasses.clear();
        gradeOfClasses.addAll(list);
        grades_adapter.notifyDataSetChanged();

        status = "null";
        getActivity().setTitle(getResources().getString(R.string.navigation_grades));

        if (lvAG != null) {
            lvAG.setVisibility(View.VISIBLE);
            lvSG.setVisibility(View.INVISIBLE);
        }
    }

    private void updateSingleGradeList(ArrayList<Grade> grades) {
        single_grades.clear();
        single_grades.addAll(grades);
        single_grades_adapter.notifyDataSetChanged();

        status = grades.get(0).getClassName().getName();
        getActivity().setTitle(status + " " + getResources().getString(R.string.navigation_grades));

        if (lvAG != null) {
            lvAG.setVisibility(View.INVISIBLE);
            lvSG.setVisibility(View.VISIBLE);
        }
    }

    private void openDialog(Grade grade) {
        prepareDialog("Modify", grade).create().show();
    }

    private void openDialog() {
        prepareDialog("Add", null).create().show();
    }

    private AlertDialog.Builder prepareDialog(String type, final Grade grade) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(type + " grade");

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_manage_grades, null);

        final EditText exam = view.findViewById(R.id.exam_editText);
        final EditText value = view.findViewById(R.id.grade_editText);
        final EditText weight = view.findViewById(R.id.weight_editText);

        final Spinner className = view.findViewById(R.id.class_spinner);
        className.setEnabled(true);

        ArrayList<String> classNames = new ArrayList<>();
        for (ClassName cls : mDatabaseHelper.getAllClasses()) {
            classNames.add(cls.getName());
        }
        final String[] classes = new String[classNames.size()];
        for (int i = 0; i < classNames.size(); i++) {
            classes[i] = classNames.get(i);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, classes);
        className.setAdapter(adapter);

        if (grade != null) {
            String tempClassName = grade.getClassName().getName();
            exam.setText(grade.getExam());
            value.setText(""+grade.getValue());
            weight.setText(""+grade.getWeight());

            int pos = 0;
            for (int i = 0; i < classes.length; i++) {
                if (classes[i].equals(tempClassName)) {
                    pos = i;
                    break;
                }
            }

            className.setSelection(pos);
        }
        if (!status.equals("null")) {
            int pos = 0;
            for (int i = 0; i < classes.length; i++) {
                if (classes[i].equals(status)) {
                    pos = i;
                    break;
                }
            }
            className.setSelection(pos);
            className.setEnabled(false);
        }

        builder.setView(view);
        builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String c = (String) className.getSelectedItem();
                String e = exam.getText().toString();
                String v = value.getText().toString();
                String w = weight.getText().toString();
                if (e.trim().equals("")) {
                    e = "Unnamed exam";
                }
                String origin = e;
                boolean validation = false;
                int num = 1;
                if (grade != null) {
                    if (grade.getExam().equals(e)) {
                        validation = true;
                    }
                }
                while (!validation) {
                    try {
                        if (mDatabaseHelper.getGradeFromName(e) != null) {
                            e = origin + num;
                            num++;
                        } else {
                            validation = true;
                        }
                    } catch (Exception ignored) {}
                }

                int class_fk = 1;
                for (ClassName cN : mDatabaseHelper.getAllClasses()) {
                    if (cN.getName().equals(c)) {
                        class_fk = cN.getId();
                        break;
                    }
                }
                double val;
                try {
                    val = Double.parseDouble(v);
                } catch (Exception ignored) {
                    val = 1.0;
                }
                double wei;
                try {
                    wei = Double.parseDouble(w);
                } catch (Exception ignored) {
                    wei = 1.0;
                }
                if (grade == null) {
                    addGrade(class_fk, e, val, wei);
                } else {
                    modifyGrade(grade.getId(), e, val, wei);
                }
                if (status.equals("null")) {
                    updateGradeList();
                } else {
                    updateSingleGradeList(mDatabaseHelper.getGradesFromClass(mDatabaseHelper.getClassFromName(status)).getGrades());
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        return builder;
    }

    private void addGrade(int class_fk, String exam, double grade, double weight) {
        mDatabaseHelper.addGrade(class_fk, exam, grade, weight);
    }

    private void modifyGrade(int id, String exam, double grade, double weight) {
        mDatabaseHelper.updateGrade(id, exam, grade, weight);
    }

    class CustomGradesAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return gradeOfClasses.size();
        }

        @Override
        public Object getItem(int i) {
            return gradeOfClasses.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final GradeOfClass gradeOfClass = gradeOfClasses.get(i);

            View newView = getLayoutInflater().inflate(R.layout.grades_listview_template, null);
            ConstraintLayout cl_grade_color = newView.findViewById(R.id.cl_grade_color);
            TextView tv_grade_name = newView.findViewById(R.id.tv_grade_name);
            TextView tv_grade_average = newView.findViewById(R.id.tv_grade_average);

            if (gradeOfClass.getGrades().size() > 0) {
                cl_grade_color.setBackgroundColor(getResources().getColor(gradeOfClass.getGrades().get(0).getClassName().getColor(), getActivity().getTheme()));
                tv_grade_name.setText(gradeOfClass.getGrades().get(0).getClassName().getName());
                tv_grade_average.setText("" + gradeOfClass.getAverage());
            }

            return newView;
        }
    }

    class CustomSingleGradesAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return single_grades.size();
        }

        @Override
        public Object getItem(int i) {
            return single_grades.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final Grade grade = single_grades.get(i);

            View newView = getLayoutInflater().inflate(R.layout.single_grades_listview_template, null);
            ConstraintLayout cl_single_grade_color = newView.findViewById(R.id.cl_single_grade_color);
            TextView tv_single_grade_title = newView.findViewById(R.id.tv_single_grade_title);
            TextView tv_single_grade_desc = newView.findViewById(R.id.tv_single_grade_desc);
            TextView tv_single_grade_value = newView.findViewById(R.id.tv_single_grade_value);

            cl_single_grade_color.setBackgroundColor(getResources().getColor(grade.getClassName().getColor(), getActivity().getTheme()));
            tv_single_grade_title.setText(grade.getExam());
            tv_single_grade_desc.setText(getResources().getString(R.string.weight) + ": " + grade.getWeight());
            tv_single_grade_value.setText(""+grade.getValue());

            return newView;
        }
    }
}
